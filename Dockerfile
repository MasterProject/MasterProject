# Java for i386
FROM openjdk:8-jdk-alpine
# Java for Raspberry Pi
# FROM hypriot/rpi-java
VOLUME /tmp
ADD target/MasterProject-0.1.0.jar /app.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar