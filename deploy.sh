#!/bin/sh
echo $1 >> /var/webhook/test

if [ -f /var/run/webhook/docker-gitlab-masterproject.cid ]; then
  docker stop `cat /var/run/webhook/docker-gitlab-masterproject.cid`
  rm /var/run/webhook/docker-gitlab-masterproject.cid
fi
docker login -u karstenreuter@me.com -p <LOGIN_TOKEN> registry.gitlab.com
docker pull registry.gitlab.com/lorez/masterproject
docker run -d -p 8080:8080 --rm --cidfile /var/run/webhook/docker-gitlab-masterproject.cid registry.gitlab.com/lorez/masterproject