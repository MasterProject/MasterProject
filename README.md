# MasterProject

MasterProject allows you to convert MindMeister mindmaps to MS Project files in XML format.
It accepts mindmaps in the MindMeister format. An example map can be found [here](https://mm.tt/997166198?t=6QHYQ6hLNS) and is included in the examples folder.

![Example Mindmap](images/mindmap.png)

The converter supports the following features.

* Work Breakdown Structure (WBS): The tree in the mindmap defines the WBS.
* Tasks: If task information is provided the following fields are preserved:
    - Start Date
    - Due Date (as Finish Date)
    - Resource
    - Effort (hours or days)
* Connections: Connections are used to define predecessors. The connection points from a task to its predecessor. In addition the connection label can indicate relation type and lag.
    - FF: Finish-Finish
    - FS: Finish-Start
    - SF: Start-Finish
    - SS: Start-Start
    - Lag: add lag with + or - a number and the unit (hours (h) or days (d))
        - `FS+5d`
        - `FF-4h`
        - `SF`
* Config: The Config top level task is not included in the WBS it is used for configurations. Configuration parameters are in the following format `parameter-name: parameter-value`
    - Resources: Here you can configure additional data for resources. Children represent resources.
        - capacity: The capacity of the resource in percent. Can be larger than 100%.
            - `capacity: 80%`
            - `capacity: 200%`

If you run the spring boot application locally the upload form (`resource/static/index.html`) is accessible via:

[http://localhost:8080/](http://localhost:8080/)

It is also possible to call the MapController directly via curl

`curl -X POST -F "map=@<map.mind>" http://localhost:8080/map/convert > project.xml

(replace `<map.mind> with the path to your mindmap)

## Librarys

Librarys I used for this project.

### MPXJ

[MPXJ](http://localhost:8080/) provides functions to create and write MS Project files.

### Spring Boot

I use Spring Boot to create a web application so that mindmaps can be converted via an HTML upload form.

### Jackson

I use the Jackson JSONParser to Parse the MindMeister Mindmap and create and object tree.
For this application only the objects:

- Connection
- Map
- MapItem
- Task

are relevant.

## Tools

Tools I used for this project.

## Gitlab

I use Gitlab as git repository and as CI tool. I included a .gitlab-ci.yml which I use to build a docker container and to deploy it to my Raspberry Pi at home (see Webhook).

## Docker

The app is packaged into a docker container with Spring Boot. I use the openjdk:8-jdk-alpine image which works on my Mac and on a Raspberry Pi.

### Webhook Deamon for Linux

I use a webhook to trigger deployment on a Raspberry Pi. I use the webhook deamon by Adnan Hajdarevic. I also provided a sample webhook.conf. There are a few placeholders in <> that need to be adapted to your environment.

Link: https://github.com/adnanh/webhook