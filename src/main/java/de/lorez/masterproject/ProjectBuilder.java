package de.lorez.masterproject;

import de.lorez.masterproject.data.Connection;
import de.lorez.masterproject.data.Map;
import de.lorez.masterproject.data.MapItem;
import net.sf.mpxj.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProjectBuilder {

    private final static Logger LOGGER = Logger.getLogger(ProjectBuilder.class.getName());

    public final static String CONFIG_ITEM_NAME = "Config";
    public final static String CONFIG_RESOURCES_ITEM_NAME = "Resources";

    private HashMap<Integer, Integer> taskMap = new HashMap();
    private HashMap<String, Resource> resourceMap = new HashMap();

    public ProjectFile buildProjectFromMap(Map map) {
        ProjectFile project = new ProjectFile();
        Task rootTask = project.addTask();

        MapItem rootItem = map.getRoot();
        rootTask.setName(rootItem.getTitle());
        rootTask.setStart(rootItem.getTask().getFrom());

        taskMap.put(rootItem.getId(), rootTask.getID());

        ArrayList<MapItem> children = new ArrayList<>(rootItem.getChildren());

        MapItem configItem = getConfigItem(children);

        if (configItem != null) {
            children.remove(configItem);
        }

        addChildren(rootTask, children);
        addConnections(project, map.getConnections());
        addConfig(project, configItem);

        return project;
    }

    protected MapItem getConfigItem(List<MapItem> itemList) {
        MapItem configItem = null;
        for (MapItem item: itemList) {
            if (CONFIG_ITEM_NAME.equalsIgnoreCase(item.getTitle())) {
                configItem = item;
                break;
            }
        }

        return configItem;
    }

    protected void addChildren(Task task, List<MapItem> children) {
        for (MapItem item: children) {
            Task childTask = task.addTask();
            childTask.setName(item.getTitle());

            if (item.getTask() != null) {
                childTask.setStart(item.getTask().getFrom());
                childTask.setFinish(item.getTask().getUntil());

                String resourceString = item.getTask().getResource();
                Resource resource = resourceMap.get(resourceString);
                if (resource == null) {
                    resource = task.getParentFile().addResource();
                    resource.setName(resourceString);
                    resource.setCanLevel(true);
                    resourceMap.put(resourceString, resource);
                }
                String effort = item.getTask().getEffort();
                if (effort != null) {
                    Duration effortDuration = parseDuration(effort);
                    childTask.setWork(effortDuration);
                    childTask.setRemainingWork(effortDuration);
                    childTask.setManualDuration(effortDuration);
                    childTask.setDuration(effortDuration);
                    childTask.setRemainingDuration(effortDuration);
                    childTask.setEffortDriven(true);

                    ResourceAssignment resourceAssignment = task.getParentFile().newResourceAssignment(childTask);
                    resourceAssignment.setResourceUniqueID(resource.getUniqueID());
                    resourceAssignment.setWork(effortDuration);
                    resourceAssignment.setRemainingWork(effortDuration);
                    childTask.addResourceAssignment(resourceAssignment);
                }
            }

            taskMap.put(item.getId(), childTask.getID());

            addChildren(childTask, item.getChildren());
        }
    }

    protected void addConnections(ProjectFile project, List<Connection> connections) {
        Pattern connectionPattern = Pattern.compile("([fs][fs])(([+\\-]\\d)(d|h))?", Pattern.CASE_INSENSITIVE);

        for (Connection connection: connections) {
            RelationType type = RelationType.FINISH_START;
            Duration duration = Duration.getInstance(0, TimeUnit.DAYS);

            Integer fromId = taskMap.get(connection.getFrom_id());
            Integer toId = taskMap.get(connection.getTo_id());
            Task fromTask = project.getTaskByID(fromId);
            Task toTask = project.getTaskByID(toId);

            String connectionLabel = connection.getLabel();
            if (connectionLabel != null) {
                Matcher connectionMatcher = connectionPattern.matcher(connectionLabel);
                if (connectionMatcher.matches()) {
                    String relationString = connectionMatcher.group(1).toUpperCase();

                    LOGGER.info(String.format("Connection: From %d to %d %s %s %s", fromId, toId, relationString, connectionMatcher.group(3), connectionMatcher.group(4)));

                    switch (relationString) {
                        case "FF":
                            type = RelationType.FINISH_FINISH;
                            break;
                        case "FS":
                            type = RelationType.FINISH_START;
                            break;
                        case "SF":
                            type = RelationType.START_FINISH;
                            break;
                        case "SS":
                            type = RelationType.START_START;
                    }
                    if (connectionMatcher.group(2) != null) {
                        Integer lag = new Integer(connectionMatcher.group(3));
                        TimeUnit lagUnit = TimeUnit.DAYS;

                        switch (connectionMatcher.group(4).toLowerCase()) {
                            case "d":
                                lagUnit = TimeUnit.DAYS;
                                break;
                            case "h":
                                lagUnit = TimeUnit.HOURS;
                        }
                        duration = Duration.getInstance(lag, lagUnit);
                    }
                }
            }
            fromTask.addPredecessor(toTask, type, duration);
        }
    }

    protected void addConfig(ProjectFile project, MapItem configItem) {
        for (MapItem item: configItem.getChildren()) {
            // Process Resources
            if (CONFIG_RESOURCES_ITEM_NAME.equalsIgnoreCase(item.getTitle())) {
                for (MapItem resourceItem: item.getChildren()) {
                    Resource resource = resourceMap.get(resourceItem.getTitle());
                    if (resource != null) {
                        parseResourceConfig(resource, resourceItem);
                    }
                }
            }
        }
    }

    protected void parseResourceConfig(Resource resource, MapItem resourceItem) {
        Pattern capacityPattern = Pattern.compile("capacity:\\s*(\\d+)\\s*%", Pattern.CASE_INSENSITIVE);

        for (MapItem item : resourceItem.getChildren()) {
            String title = item.getTitle();
            Matcher capacityMatcher = capacityPattern.matcher(title);
            if (capacityMatcher.matches()) {
                resource.setMaxUnits(new Integer(capacityMatcher.group(1)));
                LOGGER.info(String.format("Set MaxUnits for Resource: %s to %d", resource.getName(), resource.getMaxUnits()));
            }
        }
    }

    protected Duration parseDuration(String durationString) {
        Duration duration = Duration.getInstance(0, TimeUnit.DAYS);

        Pattern pattern = Pattern.compile("([0-9\\.]+)(hours|days)");
        Matcher matcher = pattern.matcher(durationString);
        if (matcher.matches()) {
            String number = matcher.group(1);
            String unit = matcher.group(2);

            switch (unit) {
                case "hours":
                    duration = Duration.getInstance(new Double(number), TimeUnit.HOURS);
                    break;
                case "days":
                    duration = Duration.getInstance(new Double(number), TimeUnit.DAYS);
            }
        }
        return duration;
    }
}
