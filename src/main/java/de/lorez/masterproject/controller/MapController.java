package de.lorez.masterproject.controller;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.lorez.masterproject.ProjectBuilder;
import de.lorez.masterproject.data.Map;
import net.sf.mpxj.ProjectFile;
import net.sf.mpxj.mspdi.MSPDIWriter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@RestController
@SuppressWarnings("unused")
public class MapController {

    private final static Logger LOGGER = Logger.getLogger(MapController.class.getName());

    @RequestMapping(value = "/map/convert", method = RequestMethod.POST, produces = "application/msproject")
    public @ResponseBody ResponseEntity convertMapToProject(@RequestParam("map") MultipartFile mapFile)  throws IOException {
        ZipInputStream zis = new ZipInputStream(mapFile.getInputStream());
        if (zis.available() > 0) {
            ZipEntry entry = zis.getNextEntry();

            JsonFactory jfactory = new JsonFactory();
            JsonParser jParser = jfactory.createParser(zis);

            ObjectMapper projectMapper = new ObjectMapper();
            try {
                Map map = projectMapper.readValue(jParser, Map.class);

                ProjectBuilder projectBuilder = new ProjectBuilder();
                ProjectFile project = projectBuilder.buildProjectFromMap(map);

                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                MSPDIWriter writer = new MSPDIWriter();
                writer.write(project, outputStream);
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.add("content-disposition", "attachment; filename=project.xml");
                return new ResponseEntity(outputStream.toByteArray(), responseHeaders, HttpStatus.OK);
            } catch (Exception ex) {
                LOGGER.log(Level.SEVERE, "Cannot convert Mindmap: ", ex);
            }
        }

        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}