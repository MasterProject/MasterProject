package de.lorez.masterproject.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@SuppressWarnings("unused")
public class StatusController {

    @RequestMapping("/status/")
    public String index() {
        return "OK!";
    }
}