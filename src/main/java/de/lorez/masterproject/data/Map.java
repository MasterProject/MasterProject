package de.lorez.masterproject.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Map {

    private MapItem root;

    private List<Connection> connections;

    public MapItem getRoot() {
        return root;
    }

    public void setRoot(MapItem root) {
        this.root = root;
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }

    @Override
    public String toString() {
        return root.toString();
    }
}
